class ScreenArguments {
  final String title;
  final String image;

  ScreenArguments(this.title, this.image);
}